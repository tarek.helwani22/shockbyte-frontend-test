/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        'dark-charcoal-gray': '#1E1F25',
        'very-dark-gray': '#131517',
        'periwinkle-blue': '#5051F9'
      },
      fontFamily: {
        'inter': ['Inter', 'sans'],
        'poppins': ['Poppins', 'sans'],
      }
    },
  },
  plugins: [],
}

