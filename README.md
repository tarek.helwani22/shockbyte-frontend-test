# Shockbyte frontend test

## Installation

First clone the project

```shell
git clone <repo_link>
```

Then you should install the packages:

```shell
npm install
```

Copy .env.example to create .env file (the values are filled since it's a test and there's no sesitive data)

```shell
cp .env.example .env
```

run the project in development mode:

```shell
npm run dev
```

## Structure

Let's explain the structure briefly
- assets

Here we put the assets of the project

- components

Here we put the global components (Pagination, etc .. )

- config

Here we import the config for the project (probably .env variables)

- router

config the routes

- views

Here we put the pages of the app 

## Things that I could improve more

- More abstraction to the code (but I didn't want to over-engineer the project)
- I believe there's a more efficient way to reduce the file size in Vue 3, but as a newcomer to Vue, I'm not yet familiar with the conventions for doing so.

## Thank You!

And finally I want to express my sincere gratitude for providing me with the opportunity to work on this test




