import "@/assets/main.css"

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import "./index.css"

import VueAwesomePaginate from "vue-awesome-paginate";
import "vue-awesome-paginate/dist/style.css";

import VueApexCharts from "vue3-apexcharts";

import VueToast from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css"; // Choose a theme that fits your project

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.use(VueAwesomePaginate)
app.use(VueApexCharts);

app.use(VueToast);

app.mount('#app')
