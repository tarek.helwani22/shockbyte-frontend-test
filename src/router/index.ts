import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView/HomeView.vue'
import BirdhousesView from '../views/BirdhousesView/BirdhousesView.vue'
import BirdhouseView from '../views/BirdhouseView/BirdhouseView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/birdhouses',
      name: 'birdhouses',
      component: BirdhousesView
    },
    {
      path: '/birdhouse/:id',
      name: 'birdhouse',
      component: BirdhouseView
    },
  ]
})

export default router
