import Axios from "axios";
import { BACKEND_API_URL } from "@/config";
import { useToast } from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';

export const axios = Axios.create({
    baseURL: BACKEND_API_URL,
    headers: {
        Accept: "application/json",
        "Content-type": "application/json",
    },
});

const $toast = useToast();

axios.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        const message = error.response?.message || error.message;

        $toast.error(message)

        return Promise.reject(error);
    }
);