import { Birdhouse } from "@/views/BirdhousesView/types/models";

export interface GetBirdhousesResponse {
    data: Birdhouse[]
    total: number
}