export interface Birdhouse {
    id: string
    name: string
    longitude: number
    latitude: number
    birds: number
    eggs: number
}