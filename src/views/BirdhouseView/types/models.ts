export interface BirdhouseInfo {
    name: string
    longitude: number
    latitude: number
}

export interface ResidencyData {
    timestamp: Date
    birds: number
    eggs: number
}