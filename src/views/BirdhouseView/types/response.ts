import { ResidencyData } from "@/views/BirdhouseView/types/models";

export interface GetResidencyDataResponse {
    data: ResidencyData[]
    total: number
}

export interface GetBirdhouseInfoResponse {
    name: string
    longitude: number
    latitude: number
}

export type GetGraphResidencyDataResponse = ResidencyData[]