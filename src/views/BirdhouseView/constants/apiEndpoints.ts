export const GET_BIRDHOUSE_INFO_ENDPOINT = '/birdhouses/:id'
export const GET_RESIDENCY_DATA_ENDPOINT = '/birdhouses/:id/residency';
export const GET_GRAPH_RESIDENCY_DATA_ENDPOINT = '/birdhouses/:id/graph-residency';
