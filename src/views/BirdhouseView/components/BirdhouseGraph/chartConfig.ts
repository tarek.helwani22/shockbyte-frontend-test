import { daysOfWeek } from "@/views/BirdhouseView/components/BirdhouseGraph/consts";

const categories = daysOfWeek
const labelColor = 'rgba(255, 255, 255, 0.6)';

const xLabelColors = Array(categories.length).fill(labelColor);
const yLabelColors = Array(5).fill(labelColor)

export const chartOptions = {
    chart: {
        type: "line",
        stacked: false,
        toolbar: {
            show: false
        }
    },
    xaxis: {
        categories,
        axisBorder: {
            show: false
        },
        labels: {
            style: {
                colors: xLabelColors
            }
        },
    },
    yaxis: {
        labels: {
            style: {
                colors: yLabelColors
            }
        },
    },
    stroke: {
        curve: 'smooth'
    },
    colors: ['#744F99', '#0E9CFF'],
    legend: {
        show: false
    },
    dataLabels: {
        enabled: false
    },
    grid: {
        borderColor: 'rgba(255, 255, 255, 0.2)',
        strokeDashArray: 3,
        xaxis: {
            lines: {
                show: true,
            },
        },
    },
    markers: {
        size: 3,
        fillOpacity: 1,
        shape: "circle",
    },
};